# GPG - Yubikey setup instructions

## Starting with a new yubikey 5 NFC

```
    sstam ~ $ gpg --card-status
    Reader ...........: Yubico Yubikey 4 OTP U2F CCID 00 00
    Application ID ...: D2760001240103040006120837950000
    Version ..........: 3.4
    Manufacturer .....: Yubico
    Serial number ....: 12083795
    Name of cardholder: [not set]
    Language prefs ...: [not set]
    Sex ..............: unspecified
    URL of public key : [not set]
    Login data .......: [not set]
    Signature PIN ....: not forced
    Key attributes ...: rsa2048 rsa2048 rsa2048
    Max. PIN lengths .: 127 127 127
    PIN retry counter : 3 0 3
    Signature counter : 0
    Signature key ....: [none]
    Encryption key....: [none]
    Authentication key: [none]
    General key info..: [none]
```

### Lets try to Create a GPG key from scratch and set up the key using the yubikey apps

#### Create a new GPG master key that does not expire and create 3 sub keys (automatically signed by the master)

The GnuPG Implementation of PGP automatically creates an encryption subkey under the master key in order to provide functionalities like PGP mail etc out of the box. Keep in mind GnuPG uses a keying system and understands what key to use every time automatically.

Source: https://wiki.debian.org/Subkeys?action=show&redirect=subkeys

1. Make sure you install gpg 2 (probably comes pre-installed with linux flavoured distros)

2. Create a temp gpg home folder or use the default home folder .gnupg without doing anything. 

```
    sstam ~ $ mkdir gpg-test
    sstam ~ $ export GNUPGHOME=~/gpg-test
    sstam ~ $ echo $GNUPGHOME
    /home/sstam/gpg-test
    sstam ~ $ gpg --list-keys 
    gpg: WARNING: unsafe permissions on homedir '/home/sstam/gpg-test'
    gpg: keybox '/home/sstam/gpg-test/pubring.kbx' created
    gpg: /home/sstam/gpg-test/trustdb.gpg: trustdb created
    sstam ~ $ chmod 700 ~/gpg-test
    sstam ~ $ chmod 600 ~/gpg-test/*
```

3. Create your non-expiring master key with Signing and Certification capabilities, RSA 4096 bit length, set your identity using your name, email and an optional comment in this case (Main key). **Use a strong passphrase that you will not forget. It can later be changed.**

```
    sstam gpg-test $ gpg --expert --full-generate-key
    gpg (GnuPG) 2.2.4; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Please select what kind of key you want:
    (1) RSA and RSA (default)
    (2) DSA and Elgamal
    (3) DSA (sign only)
    (4) RSA (sign only)
    (7) DSA (set your own capabilities)
    (8) RSA (set your own capabilities)
    (9) ECC and ECC
    (10) ECC (sign only)
    (11) ECC (set your own capabilities)
    (13) Existing key
    Your selection? 8

    Possible actions for a RSA key: Sign Certify Encrypt Authenticate 
    Current allowed actions: Sign Certify Encrypt 

    (S) Toggle the sign capability
    (E) Toggle the encrypt capability
    (A) Toggle the authenticate capability
    (Q) Finished

    Your selection? E

    Possible actions for a RSA key: Sign Certify Encrypt Authenticate 
    Current allowed actions: Sign Certify 

    (S) Toggle the sign capability
    (E) Toggle the encrypt capability
    (A) Toggle the authenticate capability
    (Q) Finished

    Your selection? Q
    RSA keys may be between 1024 and 4096 bits long.
    What keysize do you want? (3072) 4096
    Requested keysize is 4096 bits
    Please specify how long the key should be valid.
            0 = key does not expire
        <n>  = key expires in n days
        <n>w = key expires in n weeks
        <n>m = key expires in n months
        <n>y = key expires in n years
    Key is valid for? (0) 0
    Key does not expire at all
    Is this correct? (y/N) y

    GnuPG needs to construct a user ID to identify your key.

    Real name: Foo Bar
    Email address: foo.bar@example.com
    Comment: Main key
    You selected this USER-ID:
        "Foo Bar (Main key) <foo.bar@example.com>"

    Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.
    gpg: key 323F830C19D743C4 marked as ultimately trusted
    gpg: directory '/home/sstam/gpg-test/openpgp-revocs.d' created
    gpg: revocation certificate stored as '/home/sstam/gpg-test/openpgp-revocs.d/DF3127D661FB188F3225FC6D323F830C19D743C4.rev'
    public and secret key created and signed.

    Note that this key cannot be used for encryption.  You may want to use
    the command "--edit-key" to generate a subkey for this purpose.
    pub   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
    uid                      Foo Bar (Main key) <foo.bar@example.com>
```

Notice the files created inside the gpg folder (in our case `~/gpg-test/`):
 - `openpgp-revocs.d/DF3127D661FB188F3225FC6D323F830C19D743C4.rev`
   - This revocation certificate must be backed-up in case you need to revoke your key. This is useful when your key is published in a public key server and later compromised. This is the only way to mark it as obsolete.
 - `private-keys-v1.d/ACAC7584080D47979BA7991FA93DF33FD873D7D3.key`
   - This is the private key file and should be removed at the end or moved to the yubikey. Make sure you backup this file along with the rest of the file structure for a quick recovery.
   - More keys will be created under this folder in the following steps

```
    gpg --list-secret-keys  --with-keygrip
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    sec   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
        Keygrip = ACAC7584080D47979BA7991FA93DF33FD873D7D3
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>
```

List the keys:

```
    sstam gpg-test $ gpg --list-public-keys
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    pub   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>

    sstam gpg-test $ gpg --list-secret-keys
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    sec   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>

    sstam gpg-test $ gpg --list-sigs
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    pub   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>
    sig 3        323F830C19D743C4 2020-07-18  Foo Bar (Main key) <foo.bar@example.com>
```

4. Create the rest of the keys. We need one for Authentication, one for Encryption and another one for Signing. All in all, we will end up with 4 different private keys 3 of which will be subkeys of the master key.

```
    sstam gpg-test $ gpg --edit-key foo.bar@example.com 
    gpg (GnuPG) 2.2.4; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> addkey
    Please select what kind of key you want:
    (3) DSA (sign only)
    (4) RSA (sign only)
    (5) Elgamal (encrypt only)
    (6) RSA (encrypt only)
    Your selection? 4
    RSA keys may be between 1024 and 4096 bits long.
    What keysize do you want? (3072) 4096
    Requested keysize is 4096 bits
    Please specify how long the key should be valid.
            0 = key does not expire
        <n>  = key expires in n days
        <n>w = key expires in n weeks
        <n>m = key expires in n months
        <n>y = key expires in n years
    Key is valid for? (0) 0
    Key does not expire at all
    Is this correct? (y/N) y
    Really create? (y/N) y
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> addkey
    Please select what kind of key you want:
    (3) DSA (sign only)
    (4) RSA (sign only)
    (5) Elgamal (encrypt only)
    (6) RSA (encrypt only)
    Your selection? 6
    RSA keys may be between 1024 and 4096 bits long.
    What keysize do you want? (3072) 4096
    Requested keysize is 4096 bits
    Please specify how long the key should be valid.
            0 = key does not expire
        <n>  = key expires in n days
        <n>w = key expires in n weeks
        <n>m = key expires in n months
        <n>y = key expires in n years
    Key is valid for? (0) 0
    Key does not expire at all
    Is this correct? (y/N) y
    Really create? (y/N) y
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> save
```

5. We could have started editing the key in expert mode, to do everything in one swoop. Without expert mode gpg does not let you create an Authentication key. Here is the procedure:

```
    sstam gpg-test $ gpg --edit-key --expert foo.bar@example.com 
    gpg (GnuPG) 2.2.4; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> addkey
    Please select what kind of key you want:
    (3) DSA (sign only)
    (4) RSA (sign only)
    (5) Elgamal (encrypt only)
    (6) RSA (encrypt only)
    (7) DSA (set your own capabilities)
    (8) RSA (set your own capabilities)
    (10) ECC (sign only)
    (11) ECC (set your own capabilities)
    (12) ECC (encrypt only)
    (13) Existing key
    Your selection? 8

    Possible actions for a RSA key: Sign Encrypt Authenticate
    Current allowed actions: Sign Encrypt 

    (S) Toggle the sign capability
    (E) Toggle the encrypt capability
    (A) Toggle the authenticate capability
    (Q) Finished

    Your selection? a

    Possible actions for a RSA key: Sign Encrypt Authenticate
    Current allowed actions: Sign Encrypt Authenticate

    (S) Toggle the sign capability
    (E) Toggle the encrypt capability
    (A) Toggle the authenticate capability
    (Q) Finished

    Your selection? e

    Possible actions for a RSA key: Sign Encrypt Authenticate
    Current allowed actions: Sign Authenticate

    (S) Toggle the sign capability
    (E) Toggle the encrypt capability
    (A) Toggle the authenticate capability
    (Q) Finished

    Your selection? s

    Possible actions for a RSA key: Sign Encrypt Authenticate
    Current allowed actions: Authenticate

    (S) Toggle the sign capability
    (E) Toggle the encrypt capability
    (A) Toggle the authenticate capability
    (Q) Finished

    Your selection? q
    RSA keys may be between 1024 and 4096 bits long.
    What keysize do you want? (3072) 4096
    Requested keysize is 4096 bits
    Please specify how long the key should be valid.
            0 = key does not expire
        <n>  = key expires in n days
        <n>w = key expires in n weeks
        <n>m = key expires in n months
        <n>y = key expires in n years
    Key is valid for? (0) 0
    Key does not expire at all
    Is this correct? (y/N) y
    Really create? (y/N) y
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> save
```

```
    sstam gpg-test $ gpg --list-secret-keys  --with-keygrip
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    sec   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
        Keygrip = ACAC7584080D47979BA7991FA93DF33FD873D7D3
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>
    ssb   rsa4096 2020-07-18 [S]
        Keygrip = 1D0F7794C4794183EC64FF419C34D1CAB29F2E90
    ssb   rsa4096 2020-07-18 [E]
        Keygrip = CC8D4CA7520EF9D4BE21071A87DE565BBCB43FFB
    ssb   rsa4096 2020-07-18 [A]
        Keygrip = 6D2F051E85253ECD52A3861771A1DAE1FE603456

```

Notice the private keys in the private key folder:

- ~/gpg-test/private-keys-v1.d/1D0F7794C4794183EC64FF419C34D1CAB29F2E90.key
- ~/gpg-test/private-keys-v1.d/6D2F051E85253ECD52A3861771A1DAE1FE603456.key
- ~/gpg-test/private-keys-v1.d/ACAC7584080D47979BA7991FA93DF33FD873D7D3.key
- ~/gpg-test/private-keys-v1.d/CC8D4CA7520EF9D4BE21071A87DE565BBCB43FFB.key

Lets list all the pairs again:

```
    sstam gpg-test $ gpg --list-public-keys
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    pub   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>
    sub   rsa4096 2020-07-18 [S]
    sub   rsa4096 2020-07-18 [E]
    sub   rsa4096 2020-07-18 [A]

    sstam gpg-test $ gpg --list-secret-keys
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    sec   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>
    ssb   rsa4096 2020-07-18 [S]
    ssb   rsa4096 2020-07-18 [E]
    ssb   rsa4096 2020-07-18 [A]

    sstam gpg-test $ gpg --list-sigs
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    pub   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>
    sig 3        323F830C19D743C4 2020-07-18  Foo Bar (Main key) <foo.bar@example.com>
    sub   rsa4096 2020-07-18 [S]
    sig          323F830C19D743C4 2020-07-18  Foo Bar (Main key) <foo.bar@example.com>
    sub   rsa4096 2020-07-18 [E]
    sig          323F830C19D743C4 2020-07-18  Foo Bar (Main key) <foo.bar@example.com>
    sub   rsa4096 2020-07-18 [A]
    sig          323F830C19D743C4 2020-07-18  Foo Bar (Main key) <foo.bar@example.com>
```

You might be wondering what is the meaning of the number 3 next to the type indicator. According to the man page: "**3 means you did extensive verification of the key.**"

6. Now is the time to backup, even though the master key is more important. Again back up the whole .gnupg folder. Copy to at least 2 mediums (usb, hard disk drives) and don't forget your passphrase.

#### Transfer your keys to the yubikey device

1. Download the yubikey software packages:
    - Yubikey Manager
    - Yubikey Personnalization Tool
    - Yubikey Authenticator (Optional)

2. Open the apps and insert the yubikey

3. Check Yubikey manager:
   - The home page shows the firmware and the serial number of the device.
   - Go to the Interfaces tab to enable/disable all the available features
   - In the Applications tab you can configure each feature. Make sure you change the default pin and puk. You can always reset everything if you forget the pins.

You can see some of the information by running the `gpg --card-status` command as we saw in the beggining.

Yubikey PIV is different from GPG. Don't confuse the PIV pins with the GPG ones. PIV is a similar feature.

```
    sstam gpg-test $ gpg --card-edit

    Reader ...........: 1050:0407:X:0
    Application ID ...: D2760001240103040006120837950000
    Version ..........: 3.4
    Manufacturer .....: Yubico
    Serial number ....: 12083795
    Name of cardholder: [not set]
    Language prefs ...: [not set]
    Sex ..............: unspecified
    URL of public key : [not set]
    Login data .......: [not set]
    Signature PIN ....: not forced
    Key attributes ...: rsa2048 rsa2048 rsa2048
    Max. PIN lengths .: 127 127 127
    PIN retry counter : 3 0 3
    Signature counter : 0
    Signature key ....: [none]
    Encryption key....: [none]
    Authentication key: [none]
    General key info..: [none]

    gpg/card>
```

- Default PIN is be: `123456` (**min 6 digits**)
  - 3 Failed retries will block the PIN which then must be unlocked with the AdminPIN
    - Toggle the admin commands by typing the `admin` command
    - Login as `admin` using the `login` command
    - **3 failed attempts will block the card and then a factory reset is needed, losing all the GPG data inside**
    - Unblock the PIN using `passwd` command and selecting `2 - unblock PIN`
    - Change the PIN (preferrably)
  - If the PIN or AdminPIN is entered correctly the retry failure counter will reset to 3
- Default AdminPin is be `12345678` (**min 8 digits**)
- The gpg reset code is by default disabled (middle '0' retries indicator). This code can be set, to give the regular user the ability to reset the PIN without the need of the AdminPin. (**recommended**)

Lets setup the gpg PINs:

```
    gpg/card> passwd 
    gpg: OpenPGP card no. D2760001240103040006120837950000 detected
    PIN changed.

    gpg/card> admin
    Admin commands are allowed

    gpg/card> login 
    Login data (account name): admin

    gpg/card> passwd 
    gpg: OpenPGP card no. D2760001240103040006120837950000 detected

    1 - change PIN
    2 - unblock PIN
    3 - change Admin PIN
    4 - set the Reset Code
    Q - quit

    Your selection? 4
    Reset Code set.

    1 - change PIN
    2 - unblock PIN
    3 - change Admin PIN
    4 - set the Reset Code
    Q - quit

    Your selection? 3
    Error changing the PIN: Operation cancelled

    1 - change PIN
    2 - unblock PIN
    3 - change Admin PIN
    4 - set the Reset Code
    Q - quit

    Your selection? 3
    PIN changed.

    1 - change PIN
    2 - unblock PIN
    3 - change Admin PIN
    4 - set the Reset Code
    Q - quit

    Your selection? q

    gpg/card> admin
    Admin commands are not allowed

    gpg/card> list

    Reader ...........: Yubico Yubikey 4 OTP U2F CCID 00 00
    Application ID ...: D2760001240103040006120837950000
    Version ..........: 3.4
    Manufacturer .....: Yubico
    Serial number ....: 12083795
    Name of cardholder: [not set]
    Language prefs ...: [not set]
    Sex ..............: unspecified
    URL of public key : [not set]
    Login data .......: admin
    Signature PIN ....: not forced
    Key attributes ...: rsa2048 rsa2048 rsa2048
    Max. PIN lengths .: 127 127 127
    PIN retry counter : 3 3 3
    Signature counter : 0
    Signature key ....: [none]
    Encryption key....: [none]
    Authentication key: [none]
    General key info..: [none]

```

PIN and AdminPIN have been reset and Reset Code has been set for the regular user to be able to unblock the PIN after 3 failed attempts without the help of the Admin.

You can set your name, language, sex, url of public key and whether to enable PIN prompt on Signing.

Lets transfer the gpg keys to the yubikey:

```
    sstam gpg-test $ gpg --edit-key foo.bar@example.com 
    gpg (GnuPG) 2.2.4; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> key 1

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb* rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> keytocard 
    Please select where to store the key:
    (1) Signature key
    (3) Authentication key
    Your selection? 1

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb* rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> key 1

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> key 2

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb* rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> keytocard 
    Please select where to store the key:
    (2) Encryption key
    Your selection? 2

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb* rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> key 2

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> key 3

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb* rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> keytocard 
    Please select where to store the key:
    (3) Authentication key
    Your selection? 3

    sec  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: ultimate
    ssb  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S  
    ssb  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E  
    ssb* rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A  
    [ultimate] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> save
```

List your keys and notice the symbol `>` next to each subkey.

```
    sstam gpg-test $ gpg -K
    /home/sstam/gpg-test/pubring.kbx
    --------------------------------
    sec   rsa4096 2020-07-18 [SC]
        DF3127D661FB188F3225FC6D323F830C19D743C4
    uid           [ultimate] Foo Bar (Main key) <foo.bar@example.com>
    ssb>  rsa4096 2020-07-18 [S]
    ssb>  rsa4096 2020-07-18 [E]
    ssb>  rsa4096 2020-07-18 [A]
```

If you check the private private keys folder, you will notice that the 3 subkey files have been modified, (the actual keys have been moved to the gpg smartcard and the remainders are the pointer records). You will also notice that the master private key has not been modified and is still in the folder. This needs to be deleted (make sure you did a backup in the previous step).

Verify the subkeys have been moved to the smartcard:

```
    sstam gpg-test $ gpg --card-status

    Reader ...........: Yubico Yubikey 4 OTP U2F CCID 00 00
    Application ID ...: D2760001240103040006120837950000
    Version ..........: 3.4
    Manufacturer .....: Yubico
    Serial number ....: 12083795
    Name of cardholder: [not set]
    Language prefs ...: [not set]
    Sex ..............: unspecified
    URL of public key : [not set]
    Login data .......: admin
    Signature PIN ....: not forced
    Key attributes ...: rsa4096 rsa4096 rsa4096
    Max. PIN lengths .: 127 127 127
    PIN retry counter : 3 3 3
    Signature counter : 0
    Signature key ....: DC4E 0CD2 DE57 4A83 07D3  120D 29AC 70C1 146B 779D
        created ....: 2020-07-18 18:19:38
    Encryption key....: C363 55C4 EAB8 F37D 209F  F673 1F35 B50C AB84 CFF4
        created ....: 2020-07-18 18:20:10
    Authentication key: 8C8F 764E D5A9 2C84 7F21  6EE2 4D00 EC7D 8535 D918
        created ....: 2020-07-18 18:24:17
    General key info..: sub  rsa4096/29AC70C1146B779D 2020-07-18 Foo Bar (Main key) <foo.bar@example.com>
    sec   rsa4096/323F830C19D743C4  created: 2020-07-18  expires: never
    ssb>  rsa4096/29AC70C1146B779D  created: 2020-07-18  expires: never
                                    card-no: 0006 12083795
    ssb>  rsa4096/1F35B50CAB84CFF4  created: 2020-07-18  expires: never
                                    card-no: 0006 12083795
    ssb>  rsa4096/4D00EC7D8535D918  created: 2020-07-18  expires: never
                                    card-no: 0006 12083795
```

Export your public key, remove the keys and import the public key back:

```
    sstam gpg-test $ gpg --armor --export foo.bar@example.com > foobar.asc
    sstam gpg-test $ cp foobar.asc ../
    sstam gpg-test $ sudo rm -rf $GNUPGHOME
    sstam ~ $ mkdir gpg-test
    sstam ~ $ chmode 700 gpg-test
    sstam ~ $ cd gpg-test
    sstam gpg-test $ gpg --import ../foobar.asc
    gpg: keybox '/home/sstam/gpg-test/pubring.kbx' created
    gpg: /home/sstam/gpg-test/trustdb.gpg: trustdb created
    gpg: key 323F830C19D743C4: public key "Foo Bar (Main key) <foo.bar@example.com>" imported
    gpg: Total number processed: 1
    gpg:               imported: 1
    sstam gpg-test $ chmod 600 ./*
```

Set the trust for the key to ultimate:

```
    sstam gpg-test $ gpg --edit-key foo.bar@example.com
    gpg (GnuPG) 2.2.4; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    pub  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: unknown       validity: unknown
    sub  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S
    sub  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E
    sub  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A
    [ unknown] (1). Foo Bar (Main key) <foo.bar@example.com>

    gpg> trust
    pub  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: unknown       validity: unknown
    sub  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S
    sub  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E
    sub  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A
    [ unknown] (1). Foo Bar (Main key) <foo.bar@example.com>

    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)

    1 = I don't know or won't say
    2 = I do NOT trust
    3 = I trust marginally
    4 = I trust fully
    5 = I trust ultimately
    m = back to the main menu

    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y

    pub  rsa4096/323F830C19D743C4
        created: 2020-07-18  expires: never       usage: SC  
        trust: ultimate      validity: unknown
    sub  rsa4096/29AC70C1146B779D
        created: 2020-07-18  expires: never       usage: S
    sub  rsa4096/1F35B50CAB84CFF4
        created: 2020-07-18  expires: never       usage: E
    sub  rsa4096/4D00EC7D8535D918
        created: 2020-07-18  expires: never       usage: A
    [ unknown] (1). Foo Bar (Main key) <foo.bar@example.com>
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.

    gpg> quit
```

Check the smartcard status (if you encounter any issues/errors, try opening the yubico authenticator and closing it, otherwise add "card-timeout 1" to the scdaemon.conf file, **googling expected**):

```
    sstam ~ $ sudo gpg --card-status
    gpg: WARNING: unsafe ownership on homedir '/home/sstam/.gnupg'
    Reader ...........: Yubico Yubikey 4 OTP U2F CCID 00 00
    Application ID ...: D2760001240103040006120837950000
    Version ..........: 3.4
    Manufacturer .....: Yubico
    Serial number ....: 12083795
    Name of cardholder: [not set]
    Language prefs ...: [not set]
    Sex ..............: unspecified
    URL of public key : [not set]
    Login data .......: admin
    Signature PIN ....: not forced
    Key attributes ...: rsa4096 rsa4096 rsa4096
    Max. PIN lengths .: 127 127 127
    PIN retry counter : 3 3 3
    Signature counter : 0
    Signature key ....: DC4E 0CD2 DE57 4A83 07D3  120D 29AC 70C1 146B 779D
        created ....: 2020-07-18 18:19:38
    Encryption key....: C363 55C4 EAB8 F37D 209F  F673 1F35 B50C AB84 CFF4
        created ....: 2020-07-18 18:20:10
    Authentication key: 8C8F 764E D5A9 2C84 7F21  6EE2 4D00 EC7D 8535 D918
        created ....: 2020-07-18 18:24:17
    General key info..: [none]
```

Lets try the card out:

1. Encrypt a message for yourself (in our case $KEYID is foo.bar@example.com or the key fingerprint):

`echo "test message string" | gpg --encrypt --armor --recipient $KEYID -o encrypted.txt`

2. Decrypt the message:

`gpg --decrypt --armor encrypted.txt`

If you encounter any errors at this point try:

- Re-importing the public key
- Restarting gpg services or rebooting
- Opening the Yubikey manager applications (seek support on the yubikey forums)
- Fixing permissions on your .gnupg folder

This should sum up the creation of GPG keys and the set up of the Yubikey.


Sources:

- [YubiKey-Guide](https://github.com/drduh/YubiKey-Guide#creating-keys)
- [Debian creating-key](https://keyring.debian.org/creating-key.html)
- [Yubikey 5 NFC Technical Manual](https://support.yubico.com/support/solutions/articles/15000014219-yubikey-5-series-technical-manual#Physical_Attributesv5en1t)